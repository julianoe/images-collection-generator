/*
* Fonction permettant de copier dans le presse papier l'url au clic sur le lien
*/
function copyURI(evt) {
    evt.preventDefault();
    navigator.clipboard.writeText(evt.target.getAttribute('href')).then(() => {
        /* clipboard successfully set */
    }, () => {
        /* clipboard write failed */
    });
}