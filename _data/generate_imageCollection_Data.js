const fs = require('fs');
const path = require('path');
const crypto = require('crypto');
const ExifReader = require('exifreader');

// Function to read metadata from an image file
function readMetadata(filePath) {
    try {
        // Read the image file
        const data = fs.readFileSync(filePath);
        
        // Parse the image data to extract metadata
        const tags = ExifReader.load(data);
        
        // Convert the parsed metadata to a plain object
        const metadata = {};
        for (const tag in tags) {
            if (tags.hasOwnProperty(tag)) {
                metadata[tag] = tags[tag].description;
            }
        }
        // console.log(`Metadata successfully read for ${filePath}`);
        return metadata;
    } catch (error) {
        console.warn(`Error reading metadata for ${filePath}:`);
        return {}; // Return empty object if there's an error reading metadata
    }
}

// Function to generate a unique ID for an image
function generateImageId(filePath) {
    const data = fs.readFileSync(filePath);
    const hash = crypto.createHash('sha1');
    hash.update(data);
    return hash.digest('hex').substr(0, 10);
}

// Function to generate JSON data for all images in a directory (including subdirectories)
function generateJsonFromImages(directory, options = {}) {
    const jsonData = [];

    // Function to check if a file or directory should be excluded
    function shouldBeExcluded(file) {
        if (options.excludeFolders && options.excludeFolders.includes(file)) {
            return true; // Exclude specified folders
        }
        if (options.excludeImages && options.excludeImages.includes(file)) {
            return true; // Exclude specified images
        }
        return false;
    }

    function traverse(directory) {
        const files = fs.readdirSync(directory);

        files.forEach(file => {
            const filePath = path.join(directory, file);
            const stats = fs.statSync(filePath);

            if (stats.isDirectory()) {
                // If it's a directory, check if it should be excluded
                if (!shouldBeExcluded(file)) {
                    // If not excluded, recursively traverse it
                    traverse(filePath);
                }
            } else {
                if (!shouldBeExcluded(file)) {
                    const ext = path.extname(file).toLowerCase();
                    // Check if the file is an image file
                    if (ext === '.jpg' || ext === '.jpeg' || ext === '.png' || ext === '.gif' || ext === '.webp') {
                        // Extract EXIF metadata
                        const exifData = readMetadata(filePath);
                        // Generate unique ID for the image
                        const imageId = generateImageId(filePath);
                        // Add image data to JSON array
                        jsonData.push({
                            id: imageId,
                            filename: file,
                            path: filePath, // Add relative path here
                            exif: exifData,
                        });
                    }
                }
            }
        });
    }

    traverse(directory);
    return jsonData;
}

// Path to the directory containing your images
const imageDirectory = 'images';
// Specify the folders or images to exclude
const options = {
    excludeFolders: ['perso'], // Exclude specified folders
    excludeImages: [''] // Exclude specified images
};
// Generate JSON data from images
const imageData = generateJsonFromImages(imageDirectory, options);


// Write JSON data to file
fs.writeFileSync('_data/imagesCollection.json', JSON.stringify(imageData, null, 2));
console.log('JSON data generated successfully!');
